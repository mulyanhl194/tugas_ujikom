<?php ob_start(); ?>
<html>
<head>
	<title> Cetak PDF </title>
	
	<style>
	  table {border-collapse:collapse; table-layout:fixed;width: 630px;}
	  table td {word-wrap:break-word;width: 20%}
	</style>
</head>

<body>
	<h1 style="text-align: center;">Data Petugas</h1>
	  <table border="1" width="100%">
	  
	  <tr>
	    <th> No </th>
	    <th> ID Peminjaman</th>
	    <th> Nama Barang </th>
	    <th> Jumlah </th>
	    <th> Tanggal Pinjam </th>
	    <th> Tanggal Kembali </th>
	    <th> Nama Pegawai</th>
	 </tr>
	 
	 <?php
	 include "koneksi.php";
	 $no=1;
	 $select=mysql_query("select * from peminjaman a left join pegawai b on b.id_pegawai=a.id_pegawai where status_peminjaman='Pinjam'");
	 while($data=mysql_fetch_array($select))
	 {
		 ?>
			<tr>
				<td><?php echo $no++; ?></td>
				<td><?php echo $data['id_peminjaman']; ?></td>
				<td><?php echo $data['nama']; ?></td>
				<td><?php echo $data['jumlah_pinjam']; ?></td>
				<td><?php echo $data['tanggal_pinjam']; ?></td>
				<td><?php echo $data['tanggal_kembali']; ?></td>
				<td><?php echo $data['nama_pegawai']; ?></td>
			</tr>
	 <?php
	 }
	 ?>
</table>
</body>
</html>

<?php
// Load file koneksi.php 
$html = ob_get_contents();
ob_end_clean ();

require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF ('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data.pdf', 'D');
?>
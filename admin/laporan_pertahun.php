<?php
include ('cek.php');
error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Inventaris</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->		
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="blue">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand"><span><font size="5">Inventaris Sarana & Prasarana di SMK </font></span></a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i><?php echo $_SESSION['nama_petugas'];?> 
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
								</li>
								<li><a href="logout.php"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
	<div class="container-fluid-full">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<?php
			if ($_SESSION['id_level']==1){
				
				
			echo'<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li><a href="inventarisir.php"><i class="icon-tasks"></i><span class="hidden-tablet">  Inventaris</span></a></li>
						<li>
							<a class="dropmenu" href="#"><i class="icon-tasks"></i><span class="hidden-tablet">  Lainnya</span></a>
							<ul>
								<li><a class="submenu" href="jenis.php"><i class="icon-file-alt"></i><span class="hidden-tablet"> Jenis</span></a></li>
								<li><a class="submenu" href="ruang.php"><i class="icon-file-alt"></i><span class="hidden-tablet"> Ruang</span></a></li>
								<li><a class="submenu" href="petugas.php"><i class="icon-file-alt"></i><span class="hidden-tablet"> Petugas</span></a></li>
								<li><a class="submenu" href="pegawai.php"><i class="icon-file-alt"></i><span class="hidden-tablet"> Pegawai</span></a></li>
								<li><a class="submenu" href="level.php"><i class="icon-file-alt"></i><span class="hidden-tablet"> Level</span></a></li>
							</ul>	
						</li>	
						<li><a href="peminjaman.php"><i class="icon-tasks"></i><span class="hidden-tablet">  Peminjaman</span></a></li>
						<li><a href="pengembalian.php"><i class="icon-tasks"></i><span class="hidden-tablet">  Pengembalian</span></a></li>
						<li><a href="laporan.php"><i class="icon-tasks"></i><span class="hidden-tablet">  Laporan</span></a></li>
					</ul>
				</div>
			</div><!---penutup---->';
		    
			}else if ($_SESSION['id_level']==2){
				
				echo'<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">	
						<li><a href="peminjaman.php"><i class="icon-tasks"></i><span class="hidden-tablet">  Peminjaman</span></a></li>
						<li><a href="pengembalian.php"><i class="icon-tasks"></i><span class="hidden-tablet">  Pengembalian</span></a></li>
					</ul>
				</div>
			</div><!---penutup---->';
			
			}else if ($_SESSION['id_level']==3){
				
				echo'<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">	
						<li><a href="peminjaman.php"><i class="icon-tasks"></i><span class="hidden-tablet"> Peminjaman</span></a></li>
					</ul>
				</div>
			</div><!---penutup---->';
			}
			?>
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">	
				<div class="row-fluid">
					<div class="row-fluid sortable">			
						<div class="box span12">
							<div class="box-header" data-original-title>
								<h2><i class="halflings-icon white user"></i><span class="break"></span></h2>
								<div class="box-icon">
									<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
									<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
								</div>
							</div>
							<div class="box-content">
								  <div class="panel-body">
										<div class="col-lg-5">
<label>Pilih Tahun</label>
											<form method="get">
											<select name="tahun" class="form-control m-bot15">
												<?php
												include "koneksi.php";
												//display values in combobox/dropdown
												$result = mysql_query("SELECT year(tanggal_pinjam) as tahun FROM peminjaman GROUP BY YEAR(tanggal_pinjam)");
												while($row = mysql_fetch_assoc($result))
												{
												echo "<option value='$row[tahun]'>$row[tahun]</option>";
												} 
												?>
											</select>
													<br/>
												<button type="submit" class="btn btn-outline btn-primary">Tampilkan</button>
											</form>
										</div>
								 </div>

						 <?php if(isset($_GET['tahun'])) { ?>
						 
						 <div class="box-content">
								<table class="table table-striped table-bordered bootstrap-datatable datatable">
											<thead> 
														 <tr>
															<th>No.</th>
															<th>ID Peminjaman</th>
															<th>Nama Barang</th>
															<th>Jumlah Pinjam</th>
															<th>Tanggal Pinjam</th>
															<th>Tanggal Kembali</th>
															<th>Nama Pegawai</th>
												
															</tr> 
											</thead>   
											<tbody>
											<?php
									$no=1;
										$select=mysql_query("select * from peminjaman a left join pegawai b on b.id_pegawai=a.id_pegawai
																left join detail_pinjam d on a.id_peminjaman=d.id_detail_pinjam
																left join inventaris i on i.id_inventaris=d.id_inventaris WHERE YEAR(tanggal_pinjam) = '$_GET[tahun]'");
									
												while($data=mysql_fetch_array($select))
												{
												?>
												  <tr>
													<td><?php echo $no++; ?></td>
													<td><?php echo $data['id_peminjaman']; ?></td>
													<td><?php echo $data['nama']; ?></td>
													<td><?php echo $data['jumlah']; ?></td>
													<td><?php echo $data['tanggal_pinjam']; ?></td>
													<td><?php echo $data['tanggal_kembali']; ?></td>
													<td><?php echo $data['nama_pegawai']; ?></td>
													</tr>
													<?php
												}
												?>
											</tbody>
								</table>  
								 <form method="POST" action="eksport_pdf_laporan_pertanggal.php"></td>
								 <input type="hidden" name="tanggal" value="<?php echo $_GET['tanggal']?>">
								 <button type="submit" class="btn btn-danger">Cetak PDF </button></form>
								 
								 <a href="laporan.php"><button type="submit" class="btn btn-primary">Laporan Per Tanggal </button></a>
								 <a href="laporan_perbulan.php"><button type="submit" class="btn btn-primary">Laporan Per Bulan </button></a>
								 <a href="laporan_pertahun.php"><button type="submit" class="btn btn-primary">Laporan Per Tahun </button></a>
							</div>
					
							</div>
							
							<?php
							}
							?>
						</div>
					</div>
				</div>
			</div>
				
		</div>
	</div>
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
	
	<div class="common-modal modal fade" id="common-Modal1" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-content">
			<ul class="list-inline item-details">
				<li><a href="http://themifycloud.com">Admin templates</a></li>
				<li><a href="http://themescloud.org">Bootstrap themes</a></li>
			</ul>
		</div>
	</div>
	
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2019 Aplikasi sarana & prasarana di SMK  <a href="http://themifycloud.com/downloads/janux-free-responsive-admin-dashboard-template/" alt="Bootstrap_Metro_Dashboard"></a></span>
			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="js/jquery.ui.touch-punch.js"></script>
	
		<script src="js/modernizr.js"></script>
	
		<script src="js/bootstrap.min.js"></script>
	
		<script src="js/jquery.cookie.js"></script>
	
		<script src='js/fullcalendar.min.js'></script>
	
		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>
	
		<script src="js/jquery.chosen.min.js"></script>
	
		<script src="js/jquery.uniform.min.js"></script>
		
		<script src="js/jquery.cleditor.min.js"></script>
	
		<script src="js/jquery.noty.js"></script>
	
		<script src="js/jquery.elfinder.min.js"></script>
	
		<script src="js/jquery.raty.min.js"></script>
	
		<script src="js/jquery.iphone.toggle.js"></script>
	
		<script src="js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="js/jquery.gritter.min.js"></script>
	
		<script src="js/jquery.imagesloaded.js"></script>
	
		<script src="js/jquery.masonry.min.js"></script>
	
		<script src="js/jquery.knob.modified.js"></script>
	
		<script src="js/jquery.sparkline.min.js"></script>
	
		<script src="js/counter.js"></script>
	
		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>

<?php ob_start(); ?>
<html>
<head>
	<title> Cetak PDF </title>
	
	<style>
	table {border-collapse:collapse; table-layout: fixed;width: 800px;}
	table td {word-wrap:break-word;width: 9%;}
	</style>
</head>

<body>
	<h1 style="text-align: center;">Data Inventaris</h1>
	  <table border="1" width="100%">
	  
	  <tr>
	  <th align="center"><font color="white">NO</font></th>
	  <th align="center"><font color="white">ID Inventaris</font></th>
	  <th align="center"><font color="white">Nama Barang</font></th>
	 <th align="center"><font color="white">Kondisi </font></th>
	 <th align="center"><font color="white">Keterangan</font></th>
	  <th align="center"><font color="white">Jumlah </font></th>
	   <th align="center"><font color="white">Jenis </font></th>
	  <th align="center"><font color="white">Tanggal Register </font></th>
	   <th align="center"><font color="white">Ruang</font></th>
	    <th align="center"><font color="white">Kode Inventaris</font></th>
	  <th align="center"><font color="white">Petugas</font></th>
	 </tr>
	 
	 <?php
	 include "koneksi.php";
	 $no=1;
	 $select=mysql_query("SELECT * FROM inventaris i
                            join jenis j on i.id_jenis=j.id_jenis
                            join ruang r on i.id_ruang = r.id_ruang
                            join petugas p on i.id_petugas=p.id_petugas
");
	 while($data=mysql_fetch_array($select))
	 {
		 ?>
			<tr>
				<td><?php echo $no++; ?></td>
				<td><?php echo $data['id_inventaris']; ?></td>
				<td><?php echo $data['nama']; ?></td>
				<td><?php echo $data['kondisi']; ?></td>
				<td><?php echo $data['keterangan']; ?></td>
				<td><?php echo $data['jumlah']; ?></td>
				<td><?php echo $data['nama_jenis']; ?></td>
				<td><?php echo $data['tanggal_register']; ?></td>
				<td><?php echo $data['nama_ruang']; ?></td>
				<td><?php echo $data['kode_inventaris']; ?></td>
				<td><?php echo $data['nama_petugas']; ?></td>
			</tr>
	 <?php
	 }
	 ?>
</table>
</body>
</html>

<?php
// Load file koneksi.php 
$html = ob_get_contents();
ob_end_clean ();

require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF ('L','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data.pdf', 'D');
?>
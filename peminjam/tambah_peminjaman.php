<?php
include ('cek.php');
error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Inventaris</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->		
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="blue">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand"><span><font size="5">Inventaris Sarana & Prasarana di SMK </font></span></a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i><?php echo $_SESSION['nama_petugas'];?> 
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
								</li>
								<li><a href="logout.php"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
	<div class="container-fluid-full">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">	
						<li><a href="peminjaman.php"><i class="icon-tasks"></i><span class="hidden-tablet">  Peminjaman</span></a></li>
						<li><a href="pengembalian.php"><i class="icon-tasks"></i><span class="hidden-tablet">  Pengembalian</span></a></li>
					</ul>
				</div>
			</div>
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
							<!-- start: Content -->
			<div id="content" class="span10">
				<div class="row-fluid">
					<div class="box-content">
							<div class="row-fluid sortable">
								<div class="box span12">
								
                                    <div class="box-header" data-original-title>
										<h2><i class="halflings-icon white edit"></i><span class="break"></span>Form Input</h2>
										<div class="box-icon">
											<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
											<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
										</div>
									</div>
												
									<div class="box-content">
										<?php
										$select_inventaris=mysql_query("SELECT 'id_inventaris','nama' FROM 'inventaris'");
										$select_pegawai=mysql_query("SELECT 'id_pegawai','nama_pegawai' FROM 'pegawai'");
										?>
										<form action="simpan_peminjaman.php" method="post" class="form-horizontal">
											<fieldset>
												
												<div class="control-group">
													<label class="control-label">Nama Barang</label>
													<div class="controls">
													<select name="id_inventaris" class="form-control" required="" />
													<option value="" >--pilih Nama Barang--</option>
													<?php
													include "koneksi.php";
													$select=mysql_query ("select*from inventaris");
													while ($data=mysql_fetch_array($select)){
														?>
														<option value="<?php echo $data['id_inventaris'];?>" ><?php echo $data['nama'];?></option>
														<?php }?>
													</select>
													
													</div>
												</div>
												
												
												<div class="control-group">
													<label class="control-label">Jumlah</label>
													<div class="controls">
														 <input name="jumlah" id="jumlah" type="number" required="" />
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label">Status Peminjaman</label>
													<div class="controls">
													<select name="status_peminjaman" class="form-control" required="" >
													<option value="" >--pilih Status--</option>
													<option>Pinjam</option>
													<option>Tidak Pinjam</option>
													
													</select>
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label">ID Pegawai</label>
													<div class="controls">
													<select name="id_pegawai" class="form-control" required="" />
													<option value="" >--pilih ID Pegawai--</option>
													<?php
													include "koneksi.php";
													$select=mysql_query ("select*from pegawai");
													while ($data=mysql_fetch_array($select)){
														?>
														<option value="<?php echo $data['id_pegawai'];?>" ><?php echo $data['nama_pegawai'];?></option>
														<?php }?>
													</select>
													
													</div>
												</div>
												
												
												<button type="submit" class="btn btn-primary">Simpan </button></a>
												<button type="reset" class="btn btn-primary">Reset </button></a>
												
											</fieldset>
										</form>  
									</div>
								</div><!--/span-->
							</div><!--/row-->
					</div>
				</div>
			</div>
		
		</div>
	</div>
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
	
	<div class="common-modal modal fade" id="common-Modal1" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-content">
			<ul class="list-inline item-details">
				<li><a href="http://themifycloud.com">Admin templates</a></li>
				<li><a href="http://themescloud.org">Bootstrap themes</a></li>
			</ul>
		</div>
	</div>
	
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<span style="text-align:left;float:left"> <a href="http://themifycloud.com/downloads/janux-free-responsive-admin-dashboard-template/" alt="Bootstrap_Metro_Dashboard"></a></span>
			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="js/jquery.ui.touch-punch.js"></script>
	
		<script src="js/modernizr.js"></script>
	
		<script src="js/bootstrap.min.js"></script>
	
		<script src="js/jquery.cookie.js"></script>
	
		<script src='js/fullcalendar.min.js'></script>
	
		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>
	
		<script src="js/jquery.chosen.min.js"></script>
	
		<script src="js/jquery.uniform.min.js"></script>
		
		<script src="js/jquery.cleditor.min.js"></script>
	
		<script src="js/jquery.noty.js"></script>
	
		<script src="js/jquery.elfinder.min.js"></script>
	
		<script src="js/jquery.raty.min.js"></script>
	
		<script src="js/jquery.iphone.toggle.js"></script>
	
		<script src="js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="js/jquery.gritter.min.js"></script>
	
		<script src="js/jquery.imagesloaded.js"></script>
	
		<script src="js/jquery.masonry.min.js"></script>
	
		<script src="js/jquery.knob.modified.js"></script>
	
		<script src="js/jquery.sparkline.min.js"></script>
	
		<script src="js/counter.js"></script>
	
		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>
